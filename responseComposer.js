function compose(response, content, status, contentType) {
    status = status ? status : 200;
    contentType = contentType ? contentType : 'text/plain';

    response.writeHead(status, {"Content-Type": contentType});
    response.write(content);
    response.end();
}

exports.compose = compose;