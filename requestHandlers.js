var fs = require('fs');
var responseComposer = require('./responseComposer');
var formidable = require('formidable');
var sys = require('sys');

function start(request, response) {

    fs.readFile('./uploadForm.html', 'utf8', function (err, data) {
        if (err) {
            responseComposer.compose(response, err, 500, 'text/html');
        } else {
            responseComposer.compose(response, data, 200, 'text/html');
        }
    });
}
function upload(request, response) {
    var form = new formidable.IncomingForm();

    form.parse(request, function (error, fields, files) {

        fs.rename(files.upload.path, 'C:/temp/test.png', function (error) {
            if (error) {
                //fs.unlink('/tmp/test.png');
                fs.rename(files.upload.path, 'C:/temp/test.png');
            }
        });
        response.writeHead(200, {"Content-Type": 'text/html'});
        response.write('<img src="/show" />');
        response.end();
    });
}

function show (request, response) {
    response.writeHead(200, {"Content-Type": "image/png"});
    fs.createReadStream("C:/temp/test.png").pipe(response);
}

exports.start = start;
exports.upload = upload;
exports.show = show;