var responseComposer = require('./responseComposer');

function route(request, response, handle, pathname) {

    if (typeof handle[pathname] === 'function') {
        handle[pathname](request, response);
    } else {
        responseComposer.compose(response, '404 - Not Found');
    }
}

exports.route = route;